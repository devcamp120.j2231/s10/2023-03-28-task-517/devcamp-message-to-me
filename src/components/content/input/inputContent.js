import { Component } from "react";

class InputContent extends Component {
    inputChangeHandler = (event) => {
        const value = event.target.value;

        this.props.inputMessageChangeHandlerProp(value);
    }

    buttonClickHandler = () => {
        this.props.ouputMessageChangeHandlerProp();
    }

    render() {
        const {inputMessageProp} = this.props;

        return (
            <>
                <div className='row mt-3'>
                    <div className='col-12'>
                    <label>Message cho bạn 12 tháng tới</label>
                    </div>
                </div>  
                <div className='row mt-3'>
                    <div className='col-12'>
                    <input className='form-control' value={inputMessageProp} onChange={this.inputChangeHandler}/>
                    </div>
                </div>
                <div className='row mt-3'>
                    <div className='col-12'>
                    <button className='btn btn-primary' onClick={this.buttonClickHandler}>Gửi thông điệp</button>
                    </div>
                </div>              
            </>
        )
    }
}

export default InputContent