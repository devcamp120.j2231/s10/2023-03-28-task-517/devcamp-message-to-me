import { Component } from "react";
import banner from '../../../assets/images/programing.png'

class ImageHeader extends Component {
    render() {
        return(
            <>
                <div className='row'>
                    <div className='col-12'>
                        <img src={banner} width="600px" alt="title"/>
                    </div>
                </div>  
            </>
        )
    }
}

export default ImageHeader